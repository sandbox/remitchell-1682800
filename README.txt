Email Login is a lightweight module that allows users to log in with either their username or their email address.
There are no configuration options -- simply install the module as usual and users will be able to log in with their email address in addition to their username.



This module is similar to LoginToboggan and email_registration.